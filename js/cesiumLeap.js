(function() {
    'use strict';
    let xPosition, yPosition, zoomLevelAtCircleStart, camera;
    let CesiumLeap = function(options){
        let cesiumLeap = this;
        this.ellipsoid = options.ellipsoid;
        this.scene = options.scene;
        this.controllerOptions = {
            host: options.host || '127.0.0.1',
            port: options.port || 6437,
            frameEventName: 'animationFrame'
        };
        Leap.loop( this.controllerOptions, cesiumLeap.onFrame );
    };

    CesiumLeap.prototype.onConnect = function(){
        console.log('successfully connected with leap controller!');
    };

    function _isGripped(hand) {
        return hand.grabStrength == 1.0;
    }
    function _filterGesture(gestureType, callback) {
        return function(frame, gesture) {
            if(gesture.type == gestureType) {
                callback(frame, gesture);
            }
        }
    }
    function _isClockwise(frame, gesture) {
        let clockwise = false,
            pointableId = gesture.pointableIds[0],
            direction = frame.pointable(pointableId).direction,
            dotProduct = Leap.vec3.dot(direction, gesture.normal);

        if (dotProduct  >  0) clockwise = true;

        return clockwise;
    }

    function _zoom(frame, circleGesture) {
        if(circleGesture.pointableIds.length == 1 && frame.pointable(circleGesture.pointableIds[0]).type == 4) {
            let zoomChange = circleGesture.progress,
                cameraHeight = cesiumLeap.ellipsoid.cartesianToCartographic(camera.position).height,
                zoomDirection = _isClockwise(frame, circleGesture) ? zoomChange : -zoomChange,
                moveRate = cameraHeight / 100.0,
                mid = 175,
                normalized = (zoomChange - mid) / -100;

            if ( zoomDirection > 0 ){
                camera.moveForward(normalized * moveRate * 5);
            }else if ( zoomDirection < 0 ){
                camera.moveBackward(Math.abs(normalized) * moveRate * 5);
            }
        }
    }


    CesiumLeap.prototype.onFrame = function(frame){
        let leftHand, rightHand;
        camera = cesiumLeap.scene.camera;
        if(frame.valid && frame.hands.length > 0){
            frame.hands.forEach(function(hand){
                if(hand.type == "left"){
                    leftHand = hand;
                }
                if(hand.type == "right"){
                    rightHand = hand;
                }
            });
        }

        if (rightHand && rightHand.timeVisible > 0.75) {
            if(frame.valid && frame.gestures.length > 0){
                frame.gestures.forEach(function(gesture){
                    _filterGesture("circle", _zoom)(frame, gesture);
                });
            }
            let movement = {},
            cameraHeight = cesiumLeap.ellipsoid.cartesianToCartographic(camera.position).height,
            moveRate = cameraHeight / 100;

            // pan - x,y
            movement.x = rightHand.palmPosition[0];
            movement.y = rightHand.palmPosition[2];
            
            if (xPosition === undefined) {
                xPosition = movement.x;
                yPosition = movement.y;
            }               
            let camPos = camera.positionCartographic,
            longitude = camPos.longitude * ( 180 / Math.PI ),
            latitude = camPos.latitude * ( 180 / Math.PI ),
            height = camPos.height / Math.pow( 10, 8 );

            if ( xPosition < movement.x && movement.x - xPosition > 1 && _isGripped( rightHand ) ){
                xPosition = movement.x;
                longitude = longitude - movement.x * height;                        
            }else if( xPosition > movement.x && movement.x - xPosition < -1  && _isGripped( rightHand ) ){
                xPosition = movement.x;
                longitude = longitude + Math.abs(movement.x) * height;
            }
            if ( xPosition - movement.x > 1 || xPosition - movement.x < -1 ) {
                xPosition = undefined;
            }
            if ( yPosition < movement.y && movement.y - yPosition > 1 && _isGripped( rightHand ) ){
                yPosition = movement.y;
                latitude = latitude - movement.x * height;
            }else if( yPosition > movement.y && movement.y - yPosition < -1  && _isGripped( rightHand ) ){
                yPosition = movement.y;
                latitude = latitude + Math.abs(movement.x) * height;
            }
            if ( yPosition - movement.y > 1 || yPosition - movement.y < -1 ) {
                yPosition = undefined;
            }

            camera.setView({
                destination: Cesium.Cartesian3.fromDegrees(longitude, latitude, cameraHeight)
            });
        }
        if ( leftHand && leftHand.timeVisible > 0.75 ) {
            //pitch - pitch
            let normal = leftHand.palmNormal,
            x = leftHand.palmPosition[0],
            pitch = -1 * normal[2],
            // Math.atan2(normal.z, normal.y) * 180/math.pi + 180;
            rotate = leftHand.direction[0],
            // yaw - yaw
            yaw = -1 * normal[0];
            
            camera.lookUp(pitch / 70);

            if ( x > 0 ){
                camera.lookRight(rotate / 70);
            }else if( x < 0 ){
                camera.lookLeft(rotate / 70);
            }
        }
    };
    window.CesiumLeap = CesiumLeap;
})(window);